(function($) {
  "use strict"; // Start of use strict

  var $formLogin = $('#login-form');
  var $formLost = $('#lost-form');
  var $formRegister = $('#register-form');
  var $divForms = $('#div-forms');
  var $modalAnimateTime = 300;
  var $msgAnimateTime = 150;
  var $msgShowTime = 2000;
  var xhttp = new XMLHttpRequest();

  // jQuery for page scrolling feature - requires jQuery Easing plugin
  $(document).on('click', 'a.page-scroll', function(event) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top - 50)
    }, 1250, 'easeInOutExpo');
    event.preventDefault();
  });

  // Highlight the top nav as scrolling occurs
  $('body').scrollspy({
    target: '.navbar-fixed-top',
    offset: 51
  });

  // Closes the Responsive Menu on Menu Item Click
  $('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
  });

  // Offset for Main Navigation
  $('#mainNav').affix({
    offset: {
      top: 100
    }
  })

  // Initialize and Configure Scroll Reveal Animation
  window.sr = ScrollReveal();
  sr.reveal('.sr-icons', {
    duration: 600,
    scale: 0.3,
    distance: '0px'
  }, 200);
  sr.reveal('.sr-button', {
    duration: 1000,
    delay: 200
  });
  sr.reveal('.sr-contact', {
    duration: 600,
    scale: 0.3,
    distance: '0px'
  }, 300);

  // Initialize and Configure Magnific Popup Lightbox Plugin
  $('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
    }
  });



  // $("form").submit(function() {
  //   switch (this.id) {
  //     case "login-form":
  //       var $lg_username = $('#login_username').val();
  //       var $lg_password = $('#login_password').val();
  //       if ($lg_username == "ERROR") {
  //         msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "error", "glyphicon-remove", "Login error");
  //       } else {
  //         xhttp.open("POST", "php/admin.php", true);
  //         xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  //         xhttp.send("username="+$lg_username+"&password="+$lg_password);
  //         // msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'), "success", "glyphicon-ok", "Login got");
  //       }
  //       return false;
  //       break;
  //     case "lost-form":
  //       var $ls_email = $('#lost_email').val();
  //       if ($ls_email == "ERROR") {
  //         msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "error", "glyphicon-remove", "Send error");
  //       } else {
  //         msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "success", "glyphicon-ok", "Send OK");
  //       }
  //       return false;
  //       break;
  //     case "register-form":
  //       var $rg_username = $('#register_username').val();
  //       var $rg_email = $('#register_email').val();
  //       var $rg_password = $('#register_password').val();
  //       if ($rg_username == "ERROR") {
  //         msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "error", "glyphicon-remove", "Register error");
  //       } else {
  //         msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'), "success", "glyphicon-ok", "Register OK");
  //       }
  //       return false;
  //       break;
  //     default:
  //       return false;
  //   }
  //   return false;
  // });

  $('#login_register_btn').click(function() {
    modalAnimate($formLogin, $formRegister)
  });
  $('#register_login_btn').click(function() {
    modalAnimate($formRegister, $formLogin);
  });
  $('#login_lost_btn').click(function() {
    modalAnimate($formLogin, $formLost);
  });
  $('#lost_login_btn').click(function() {
    modalAnimate($formLost, $formLogin);
  });
  $('#lost_register_btn').click(function() {
    modalAnimate($formLost, $formRegister);
  });
  $('#register_lost_btn').click(function() {
    modalAnimate($formRegister, $formLost);
  });

  function modalAnimate($oldForm, $newForm) {
    var $oldH = $oldForm.height();
    var $newH = $newForm.height();
    $divForms.css("height", $oldH);
    $oldForm.fadeToggle($modalAnimateTime, function() {
      $divForms.animate({
        height: $newH
      }, $modalAnimateTime, function() {
        $newForm.fadeToggle($modalAnimateTime);
      });
    });
  }

  function msgFade($msgId, $msgText) {
    $msgId.fadeOut($msgAnimateTime, function() {
      $(this).text($msgText).fadeIn($msgAnimateTime);
    });
  }

  function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
    var $msgOld = $divTag.text();
    msgFade($textTag, $msgText);
    $divTag.addClass($divClass);
    $iconTag.removeClass("glyphicon-chevron-right");
    $iconTag.addClass($iconClass + " " + $divClass);
    setTimeout(function() {
      msgFade($textTag, $msgOld);
      $divTag.removeClass($divClass);
      $iconTag.addClass("glyphicon-chevron-right");
      $iconTag.removeClass($iconClass + " " + $divClass);
    }, $msgShowTime);
  }

})(jQuery); // End of use strict
